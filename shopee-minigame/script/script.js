$(document).ready(function () {
  // Mini game
  var msg = $('.modal');
  var content = $('#ctn');
  var close = $('.modal-close');

  close.on('click', function () {
    msg.removeClass('is-active');
  })

  $(function () {
    var $hat = $('.m-game__wrapper');
    var token = true;

    $('.m-game__start').on('click', function () {
      if (!token) return;
      token = false;

      var oldDeg = $hat.data('rotate') ? $hat.data('rotate') : 0;


      // var deg = Math.floor(Math.random() * 360) + 1080 + oldDeg;
      var deg = Math.floor(Math.random() * 360) + 1080 + oldDeg;

      $hat.data('rotate', deg);
      $hat.css('transform', `rotate(${deg}deg)`);

      setTimeout(function () {
        token = true;
        result = deg % 360;
        price = '';

        if (result < 60 && result >= 0) {
          price = $('.m-game__text:nth-child(1)').text(); //ok
        } else if (result < 120 && result >= 60) {
          price = $('.m-game__text:nth-child(2)').text(); // ok
        } else if (result < 180 && result >= 120) {
          price = $('.m-game__text:nth-child(3)').text();
        } else if (result < 240 && result >= 180) {
          price = $('.m-game__text:nth-child(4)').text(); //ok
        } else if (result < 300 && result >= 240) {
          price = $('.m-game__text:nth-child(5)').text();
        } else if (result <= 360 && result >= 300) {
          price = $('.m-game__text:nth-child(6)').text();
        }

        calcResult(price + ' ' + result);
      }, 1000);
    });
  });

  function calcResult(deg) {
    // alert(deg);
    content.text(deg);
    msg.addClass('is-active');
  }
});



