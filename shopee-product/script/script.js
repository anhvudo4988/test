$(document).ready(function () {

    // Count rating
    var rating_list = $("body").find("div.rating-list");
    var count_5 = rating_list.find("div.star-5").length
    var count_4 = rating_list.find("div.star-4").length
    var count_3 = rating_list.find("div.star-3").length
    var count_2 = rating_list.find("div.star-2").length
    var count_1 = rating_list.find("div.star-1").length
    var count_total = Number(count_1) + Number(count_2) + Number(count_3) + Number(count_4) + Number(count_5)
    var rating_btn_list = rating_list.find(".rating");

    var rating_btn_total_mobile = $("body").find("div.morebutton-cmt").find("p");
    rating_btn_total_mobile.text("Xem Tất Cả (" + count_total + ")")

    rating_btn_list.eq(0).find("p").text("Tất Cả (" + count_total + ")")
    rating_btn_list.eq(1).find("p").text("5 Sao (" + count_5 + ")")
    rating_btn_list.eq(2).find("p").text("4 Sao (" + count_4 + ")")
    rating_btn_list.eq(3).find("p").text("3 Sao (" + count_3 + ")")
    rating_btn_list.eq(4).find("p").text("2 Sao (" + count_2 + ")")
    rating_btn_list.eq(5).find("p").text("1 Sao (" + count_1 + ")")

    // morebutton-cmt

    // Filter rating
    $("body").on("click", "button.rating", function () {
        var a = $(this).text().trim();

        console.log(a)
        if (a.indexOf("Tất Cả") >= 0) {
            $("body").find("div.comment").show();
        }
        if (a.indexOf("5 Sao") >= 0) {
            var star_list = $("body").find("div.rating-list").find("div.star-5")
            $("body").find("div.comment").hide();
            star_list.parents("div.comment").show();
        }
        if (a.indexOf("4 Sao") >= 0) {
            var star_list = $("body").find("div.rating-list").find("div.star-4")
            $("body").find("div.comment").hide();
            star_list.parents("div.comment").show();
        }
        if (a.indexOf("3 Sao") >= 0) {
            var star_list = $("body").find("div.rating-list").find("div.star-3")
            $("body").find("div.comment").hide();
            star_list.parents("div.comment").show();
        }
        if (a.indexOf("2 Sao") >= 0) {
            var star_list = $("body").find("div.rating-list").find("div.star-2")
            $("body").find("div.comment").hide();
            star_list.parents("div.comment").show();
        }
        if (a.indexOf("1 Sao") >= 0) {
            var star_list = $("body").find("div.rating-list").find("div.star-1")
            $("body").find("div.comment").hide();
            star_list.parents("div.comment").show();
        }
    })

    //input quantity number
    var quantity_num = Number($("body").find("input.quantity-num").val());

    $("body").on("click", "button", function () {
        var btnname = $(this).attr("id");

        if (btnname == "minusbtn") {
            if (quantity_num == 1) {
            } else {
                quantity_num = quantity_num - 1;
                $("body").find("input.quantity-num").val(quantity_num);
                console.log(quantity_num);
            }
        }
        if (btnname == "plusbtn") {
            quantity_num = quantity_num + 1;
            $("body").find("input.quantity-num").val(quantity_num);
            console.log(quantity_num);
        }
    })

    //slick slide -> slider-for



    $('.slider-for').slick({
        // các settings ...
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
    });

    $("div.slider-for-content").hover(function () {
        var contentInner = $(this).html();
        var parentthumb = $("body").find("div.thumb")
        parentthumb.html(contentInner)
    })

    //slick slide -> list-products



    $('.list-products').slick({
        // các settings ...
        slidesToShow: 5,
        slidesToScroll: 1,
        // autoplay: true,
        // autoplaySpeed: 2000,
        arrows: true,
        responsive: [
            // {
            //   breakpoint: 769,
            //   settings: {
            //     slidesToShow: 2,
            //     slidesToScroll: 1
            //   }
            // },
            {
                breakpoint: 769,
                settings: "unslick"
            }

        ]
    });


    $("body").on("click", ".morebutton-fa", function () {
        var btnname = $(this).attr("id");

        var morecontent = $("body").find("div.morebutton-content");
        morecontent.show();
        $(this).hide();

        $("body").find("div.lessbutton-fa").show();
    })

    $("body").on("click", ".lessbutton-fa", function () {
        var btnname = $(this).attr("id");

        var morecontent = $("body").find("div.morebutton-content");
        morecontent.hide();
        $(this).hide();

        $("body").find("div.morebutton-fa").show();
    })

    

    // button report

    // dropdown-3dot
    
    var clickFlag = 0;
    $('body').on('click', function () {
        if (clickFlag == 0) {
            // console.log('hide element here');
            var reportcontent = $(this).find("div.dropdown-content-3dot");
            reportcontent.hide();
            /* Hide element here */
        }
        else {
            clickFlag = 0;
        }
    });

    $(".dropdown-3dot").on("click", function (e) {
        clickFlag = 1;

        var reportcontent = $(this).find("div.dropdown-content-3dot");
        reportcontent.show();
    })

});

// button ontop

    //Get the button
    var mybutton = document.getElementById("myBtn");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function () { scrollFunction() };

    function scrollFunction() {
        if (document.body.scrollTop > 600 || document.documentElement.scrollTop > 600) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    // button ontop