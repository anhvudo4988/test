// Hiển ô input và dữ liệu cũ để thay đổi + nút update
$(document).ready(function () {
    $("body").on("click", ".btn-edit", function () {
        var valtype = $(this).parents('div.item-row').find('div.head').find('div').eq(0).find('p').text();
        var valname = $(this).parents('div.item-row').find('div.head').find('div').eq(1).find('p').text();
        var valbuy = $(this).parents('div.item-row').find('div.buy').find('p').text();
        var valsell = $(this).parents('div.item-row').find('div.sell').find('p').text();
        $(this).parents('div.item-row').find('div.head').find('div').eq(0).html('<input class="text-center" name="edit_th" value="' + valtype + '">');
        $(this).parents('div.item-row').find('div.head').find('div').eq(1).html('<input class="text-center"  name="edit_th" value="' + valname + '">');
        $(this).parents('div.item-row').find('div.buy').html('<input class="text-center"  name="edit_th" value="' + valbuy + '">');
        $(this).parents('div.item-row').find('div.sell').html('<input class="text-center"  name="edit_th" value="' + valsell + '"><button class="btn-update badge badge-primary badge-pill">Update</button>');
        $(this).hide();
    });
});

// Cập nhật thông tin đã nhập từ input vào thẻ p
$(document).ready(function () {
    $("body").on("click", ".btn-update", function () {
        var valtype = $(this).parents('div.item-row').find('div.head').find('div').eq(0).find('input[name="edit_th"]').val();
        var valname = $(this).parents('div.item-row').find('div.head').find('div').eq(1).find('input[name="edit_th"]').val();
        var valbuy = $(this).parents('div.item-row').find('div.buy').find('input[name="edit_th"]').val();
        var valsell = $(this).parents('div.item-row').find('div.sell').find('input[name="edit_th"]').val();
        if (valtype != "" && valname != "" && valbuy != "" && valsell != "") {
            $(this).parents('div.item-row').find('div.head').find('div').eq(0).html('<p class="text-light h5">' + valtype + '</p>');
            $(this).parents('div.item-row').find('div.head').find('div').eq(1).html('<p class="color-gold h5">' + valname + '</p>');
            $(this).parents('div.item-row').find('div.buy').html('<p class="text-danger h1 font-weight-bold">' + valbuy + '</p>');
            $(this).parents('div.item-row').find('div.sell').html('<p class="text-danger h1 font-weight-bold">' + valsell + '</p><button class="btn-edit badge badge-primary badge-pill">Edit</button>');
        } else {
            alert("Mời bạn nhập thông tin đầy đủ !!");
        }
    })
});

// Thêm 1 hàng mới trong bảng
$(document).ready(function () {
    $("body").on("click", ".btn-add", function () {
        var content = '<div class="item-row row"><div class="head col-4 py-2 border border-white d-flex flex-column justify-content-around d-flex align-items-center justify-content-center"><div class="type"><input class="text-center" name="edit_th" placeholder="Nhập vào loại vàng" value=""></div><div class="brand"><input class="text-center" name="edit_th" placeholder="Nhập vào thương hiệu" value=""></div></div><div class="buy col-4 py-2 border border-white d-flex align-items-center justify-content-center"><input class="text-center" name="edit_th" placeholder="Nhập vào giá mua vào" value=""></div><div class="sell col-4 py-2 border border-white d-flex align-items-center justify-content-center"><input class="text-center" name="edit_th" placeholder="Nhập vào giá bán ra" value=""><button class="btn-update badge badge-primary badge-pill">Update</button></div></div>';
        $("div.head-row").after(content);
    })
});


// Lấy url nhập vào từ in put để thay đổi ảnh
$(document).ready(function () {
    $("body").on("click", "#ima_ok", function () {
        var ima_url = $(this).parents('#ima_form').find('#ima_input').val();
        var ima_src = $(this).parents('#ima_fa').find('#ima_src').attr("src", ima_url);
        // ima_src = ima_url;
        console.log(ima_src);
        $(this).hide();
        $(this).parents('#ima_form').find('#ima_input').hide();
        $(this).parents('#ima_form').find('#ima_edit').show();
    })
});

// Hiển thị ô input url ảnh và nút ok và ẩn đi nút edit
$(document).ready(function () {
    $("body").on("click", "#ima_edit", function () {
        $(this).hide();
        $(this).parents('#ima_form').find('#ima_input').show();
        $(this).parents('#ima_form').find('#ima_ok').show();
    })
});

// Đổi màu nền của input khi click chuột vào input => màu vàng
$(document).ready(function () {
    $("body").on("focus", "input.text-center", function () {
        $(this).css("background-color", "yellow");
    })
});

// Đổi màu nền của input khi click ra ngoài => màu trắng
$(document).ready(function () {
    $("body").on("blur", "input.text-center", function () {
        $(this).css("background-color", "white");
    })
});

// Lấy ngày hiện tại cập nhật mỗi 1 giây
$(document).ready(function () {
    function refreshDate() {
        var dt = new Date();
        document.getElementById("date").innerHTML = dt.toLocaleDateString();
    }
    setInterval(refreshDate, 1000);
});

// Lấy giờ hiện tại cập nhật mỗi 1 giây
$(document).ready(function () {
    function refreshTime() {
        var dt = new Date();
        document.getElementById("time").innerHTML = dt.toLocaleTimeString();
    }
    setInterval(refreshTime, 1000);
});